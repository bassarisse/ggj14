
var Credits = BaseLayer.extend({

    _buttonBPressed: false,

    init: function() {
        this._super();

        var winSize = cc.Director.getInstance().getWinSize();
        var titleItem = cc.MenuItemLabel.create(cc.LabelBMFont.create("Back", fnt_Dialogue, 170, cc.TEXT_ALIGNMENT_CENTER), this._goToTitle, this);

        var bg = cc.Sprite.create(img_Stage4Bg);
        bg.setAnchorPoint(cc.p(0, 0));

        var menu = cc.Menu.create(titleItem);

        menu.alignItemsVerticallyWithPadding(10);
        menu.setPosition(cc.p(80, 40));

        var fontColor = cc.c3b(220, 220, 220);

        var titleLabel = cc.LabelBMFont.create("CREDITS", fnt_Dialogue, winSize.width, cc.TEXT_ALIGNMENT_CENTER);
        titleLabel.setPosition(cc.p(winSize.width / 2, winSize.height * 0.8));
        titleLabel.setColor(fontColor);

        var aboutLabel = cc.LabelBMFont.create("Made for\nGlobal Game Jam 2014", fnt_Dialogue, 400, cc.TEXT_ALIGNMENT_LEFT);
        aboutLabel.setPosition(cc.p(winSize.width * 0.25, winSize.height / 2));
        aboutLabel.setColor(fontColor);

        var peopleLabel = cc.LabelBMFont.create("Alison Ramos\nBruno Assarisse\nDiego Ruggeri\nGustavo Ravagnani\nLuana Favetta\nMarcelo Raza", fnt_Dialogue, 400, cc.TEXT_ALIGNMENT_LEFT);
        peopleLabel.setPosition(cc.p(winSize.width * 0.75, winSize.height / 2));
        peopleLabel.setColor(fontColor);

        this.addChild(bg);
        this.addChild(titleLabel);
        this.addChild(aboutLabel);
        this.addChild(peopleLabel);
        this.addChild(menu);

    },

    buttonB: function(pressed) {

        if (!pressed) {
            this._buttonBPressed = false;
            return;
        }

        if (this._buttonBPressed)
            return;
        this._buttonBPressed = true;

        this._goToTitle();

    },

    _goToTitle: function() {
        cc.Director.getInstance().replaceScene(new TitleScene());
    }

});

var CreditsScene = cc.Scene.extend({

    onEnter: function () {
        this._super();
        var layer = new Credits();
        layer.init();
        this.addChild(layer);
    }

});